#ifndef RESTTABLEWITHDEFTEST_H
#define RESTTABLEWITHDEFTEST_H

#include "iconverter.h"

class RestTableDefConverter: public RestConverter
{
public:
    RestTableDefConverter();
    ~RestTableDefConverter();
    
    QString name() const;
    void restToObject(const  RestList &rests, QJsonObject *obj) const;
    void restsFromObject(const QJsonObject &obj, RestList *rests) const;
    
private:
    struct Counter {
        int count;
        Counter() : count(0) {}
    };
    
    quint64 maxCountId(const QMap<quint64, Counter> &map) const;
    
    QDate dateValue(const QJsonValue &val, const QDate &def) const;
    quint64 idValue(const QJsonValue &val, const quint64 &def) const;
};

#endif // RESTTABLEWITHDEFTEST_H

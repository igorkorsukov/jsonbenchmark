#include "table.h"

const Table::Type Table::Uint64("uint64");
const Table::Type Table::Double("double");
const Table::Type Table::Bool("bool");
const Table::Type Table::Date("date");

void Table::clear()
{
    name = QString();
    fields.clear();
    types.clear();
    rows.clear();
}

bool Table::isEqual(const Table &other) const
{
    bool ok = other.name == name;
    if (ok) {
        ok = other.fields == fields;
    }
    
    if (ok) {
        ok = other.types == types;
    }
    
    if (ok) {
        other.rows == rows;
    }
    
    return ok;
}

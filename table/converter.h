#ifndef CONVERTER_H
#define CONVERTER_H

#include <QJsonObject>
#include <QJsonValue>
#include "table.h"

class Converter
{
public:
    Converter();
    virtual ~Converter();
    
    virtual QString name() const = 0;
    virtual void tableToObject(const  Table &table, QJsonObject *obj) const = 0;
    virtual void tableFromObject(const QJsonObject &obj, Table *table) const = 0;
    
    QJsonValue toJsonValue(const QVariant &val, const Table::Type &type) const;
    QJsonValue toJsonValue(const quint64 &val) const;
    QJsonValue toJsonValue(const QDate &val) const;
    
    QVariant fromJsonValue(const QJsonValueRef &val, const Table::Type &type) const;
    quint64 fromJsonValue(const QJsonValueRef &val, const quint64 &t) const;
    QDate fromJsonValue(const QJsonValueRef &val, const QDate &t) const;
    
    bool isType(const Table::Type &type, const quint64 &t) const;
    bool isType(const Table::Type &type, const QDate &t) const;
};

#endif // CONVERTER_H

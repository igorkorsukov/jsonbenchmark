#ifndef RESTGENERATOR_H
#define RESTGENERATOR_H

#include "rests.h"

class RestGenerator
{
public:
    RestGenerator();
    ~RestGenerator();
    
    void generate(RestList *list) const;
    
private:
    
    QDate period(int idx) const;
};

#endif // RESTGENERATOR_H

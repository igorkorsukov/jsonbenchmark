#include "tablerefconverter.h"
#include <QSet>
#include <QHash>
#include <QJsonArray>
#include <QDate>
#include <QDebug>

#include <QTime>
#define TIME(str) qDebug() << str << ":" << time.elapsed() << "ms"

int MAX_REF_COUNT = 10;

TableRefConverter::TableRefConverter()
{
    
}

TableRefConverter::~TableRefConverter()
{
    
}

QString TableRefConverter::name() const
{
    return "tableref";
}

void TableRefConverter::tableToObject(const Table &table, QJsonObject *obj) const
{
    QJsonObject header;
    header["name"] = table.name;
    
    QJsonArray fields;
    QJsonArray types;
    for (int fi = 0; fi < table.fields.count(); ++fi) {
        fields.append(QJsonValue(table.fields.at(fi)));
        types.append(QJsonValue(table.types.at(fi)));
    }
    header["fields"] = fields;
    header["types"] = types;
    
    QMap<QString /*filed*/, QSet<quint64> > uint64Sets;
    QMap<QString /*filed*/, QSet<QDate> > dateSets;
    
    for (int ri = 0, rc = table.rows.count(); ri < rc; ++ri) {
        for (int fi = 0, fc = table.fields.count(); fi < fc; ++fi) {
            QVariant val = table.rows.at(ri).at(fi);
            QString field = table.fields.at(fi);
            Table::Type type = table.types.at(fi);
            
            if (Table::Uint64 == type) uint64Sets[field].insert(val.toULongLong());
            else if (Table::Date == type) dateSets[field].insert(val.toDate());
        }
    }
    
    QJsonObject refs;
    QMap<QString, QMap<quint64, int> > uint64Idxs;
    QMap<QString, QMap<QDate, int> > dateIdxs;
    
    makeValToIdx(table.fields, uint64Sets, &uint64Idxs, &refs);
    makeValToIdx(table.fields, dateSets, &dateIdxs, &refs);
    
    header["refs"] = refs;
    
    QJsonArray rows;
    for (int ri = 0, rc = table.rows.count(); ri < rc; ++ri) {
        QJsonArray row;
        for (int fi = 0, fc = table.fields.count(); fi < fc; ++fi) {
            QVariant val = table.rows.at(ri).at(fi);
            QString field = table.fields.at(fi);
            QString type = table.types.at(fi);
            
            if (uint64Idxs.contains(field)) {
                row.append(uint64Idxs.value(field).value(val.toULongLong()));
            } else if (dateIdxs.contains(field)) {
                row.append(dateIdxs.value(field).value(val.toDate()));
            } else {
                row.append(toJsonValue(val, type));
            }
        }
        rows.append(row);
    } 
    
    obj->insert("header", header);
    obj->insert("rows", rows);
}

void TableRefConverter::tableFromObject(const QJsonObject &obj, Table *table) const
{
    table->clear();
    
    QTime time;
    time.start();
    
    QJsonObject header = obj.value("header").toObject();
    table->name = header.value("name").toString();
    
    QJsonArray fields = header.value("fields").toArray();
    QJsonArray types = header.value("types").toArray();
    
    for (int fi = 0, fc = fields.count(); fi < fc; ++fi) {
        table->fields.append(fields.at(fi).toString());
        table->types.append(types.at(fi).toString());
    }
    
    TIME("fields and types");
    
    QJsonObject refs = header.value("refs").toObject();
    QMap<QString, QList<quint64> > uint64Idxs;
    QMap<QString, QList<QDate> > dateIdxs;
    
    makeValFromIdx(table->fields, table->types, refs, &uint64Idxs);
    makeValFromIdx(table->fields, table->types, refs, &dateIdxs);
    
    TIME("makeValFromIdx");
    
    QJsonArray rows = obj.value("rows").toArray();
    table->rows.reserve(rows.size());
    for (int ri = 0, rc = rows.count(); ri < rc; ++ri) {
        QJsonArray arr = rows.at(ri).toArray();
        QVariantList row;
        for (int fi = 0, fc = table->fields.count(); fi < fc; ++fi) {
            QJsonValueRef val = arr[fi];
            const QString &field = table->fields.at(fi);
            const Table::Type &type = table->types.at(fi);
            
            if (Table::Uint64 == type && uint64Idxs.contains(field)) {
                row.append(uint64Idxs[field].at(val.toInt()));
            } else if (Table::Date == type && dateIdxs.contains(field)) {
                row.append(dateIdxs[field].at(val.toInt()));
            } else {
                row.append(fromJsonValue(val, type));
            }
        }
        table->rows.append(row);
    }
    TIME("rows");
}

template <typename T>
void TableRefConverter::makeValToIdx(const QStringList &fields, const QMap<QString, QSet<T> > &sets, QMap<QString, QMap<T, int> > *idxs, QJsonObject *refs) const
{
    for (int fi = 0, fc = fields.count(); fi < fc; ++fi) {
        QString field = fields.at(fi);
        QSet<T> set = sets.value(field);
        
        if (set.count() > 0 && set.count() <= MAX_REF_COUNT) {
            QMap<T, int> idx;
            QJsonArray ref;
            
            int index = 0;
            foreach (const T &val, set) {
                idx.insert(val, index);
                ref.append(toJsonValue(val));
                ++index;
            }
            
            idxs->insert(field, idx);
            refs->insert(field, ref);
        }
    }
}

template <typename T>
void TableRefConverter::makeValFromIdx(const QStringList &fields, const QStringList &types, const QJsonObject &refs, QMap<QString, QList<T> > *idxs) const
{
    for (int fi = 0, fc = fields.count(); fi < fc; ++fi) {
        QString field = fields.at(fi);
        QString type = types.at(fi);
        if (isType(type, T())) {
            QJsonArray ref = refs.value(field).toArray();
            for (int i = 0, c = ref.count(); i < c; ++i) {
                idxs->operator [](field).append(fromJsonValue(ref[i], T()));
            } 
        }
    }
}

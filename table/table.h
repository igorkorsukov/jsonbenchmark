#ifndef TABLE_H
#define TABLE_H

#include <QString>
#include <QList>
#include <QVariant>

struct Table {
    
    typedef QString Type;
    typedef QList<Type> TypeList;
    
    static const Type Uint64;
    static const Type Double;
    static const Type Bool;
    static const Type Date;
    
    QString name;
    QStringList fields;
    TypeList types;
    QList<QVariantList> rows;
    
    void clear();
    
    bool isEqual(const Table &other) const;
};

typedef QList<Table> TableList;

#endif // TABLE_H

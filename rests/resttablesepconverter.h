#ifndef RESTTABLESEPCONVERTER_H
#define RESTTABLESEPCONVERTER_H

#include "iconverter.h"

class RestTableRefConverter : public RestConverter
{
public:
    RestTableRefConverter();
    ~RestTableRefConverter();
    
    virtual QString name() const;
    virtual void restToObject(const  RestList &rests, QJsonObject *obj) const;
    virtual void restsFromObject(const QJsonObject &obj, RestList *rests) const;
    
private:
    
    void makeValToIdx(const QSet<QDate> &set, QJsonArray *arr, QMap<QDate, int> *idx) const;
    void makeValToIdx(const QSet<quint64> &set, QJsonArray *arr, QMap<quint64, int> *idx) const;
    
    QList<QDate> toDateList(const QJsonArray &arr) const;
    QList<quint64> toIdList(const QJsonArray &arr) const;
};

#endif // RESTTABLESEPCONVERTER_H

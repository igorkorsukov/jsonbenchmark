#ifndef TABLEREFCONVERTER_H
#define TABLEREFCONVERTER_H

#include "converter.h"

class TableRefConverter : public Converter
{
public:
    TableRefConverter();
    ~TableRefConverter();
    
    virtual QString name() const;
    virtual void tableToObject(const  Table &table, QJsonObject *obj) const;
    virtual void tableFromObject(const QJsonObject &obj, Table *table) const;
    
private:
    template <typename T>
    void makeValToIdx(const QStringList &fields, const QMap<QString, QSet<T> > &sets, QMap<QString, QMap<T, int> > *idxs, QJsonObject *refs) const;
    
    template <typename T>
    void makeValFromIdx(const QStringList &fields, const QStringList &types, const QJsonObject &refs, QMap<QString, QList<T> > *idxs) const;
};

#endif // TABLEREFCONVERTER_H

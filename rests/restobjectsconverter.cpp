#include "restobjectsconverter.h"
#include <QJsonArray>

RestObjectsConverter::RestObjectsConverter()
{

}

RestObjectsConverter::~RestObjectsConverter()
{

}

QString RestObjectsConverter::name() const
{
    return "restobjects";
}

void RestObjectsConverter::restToObject(const  RestList &rests, QJsonObject *obj) const
{
    QJsonArray arr;
    foreach (const Rest &rest, rests) {
        QJsonObject o;
        o["id"] = QString::number(rest.id);
        o["period"] = rest.period.toString("yyyy-MM-dd");
        o["quantity"] = rest.quantity;
        o["idSku"] = QString::number(rest.idSku);
        o["idStore"] = QString::number(rest.idStore);
        o["idDistributor"] = QString::number(rest.idDistributor);
        o["deleted"] = rest.deleted;

        arr.append(o);
    }

    obj->insert("rests", arr);
}

void RestObjectsConverter::restsFromObject(const QJsonObject &obj, RestList *rests) const
{
    rests->clear();
    
    QJsonArray arr = obj.value("rests").toArray();
    foreach (const QJsonValue &val, arr) {
        QJsonObject o = val.toObject();
        Rest rest;
        rest.id = o.value("id").toString().toULongLong();
        rest.period = QDate::fromString(o.value("period").toString(), "yyyy-MM-dd"); 
        rest.quantity = o.value("quantity").toInt();
        rest.idSku = o.value("idSku").toString().toULongLong();
        rest.idStore = o.value("idStore").toString().toULongLong();
        rest.idDistributor = o.value("idDistributor").toString().toULongLong();
        rest.deleted = o.value("deleted").toBool();
        
        rests->append(rest);
    }
}

#include "tableconverter.h"
#include <QJsonArray>

TableConverter::TableConverter()
{
    
}

TableConverter::~TableConverter()
{
    
}

QString TableConverter::name() const
{
    return "table";
}

void TableConverter::tableToObject(const Table &table, QJsonObject *obj) const
{
    QJsonObject header;
    header["name"] = table.name;
    
    QJsonArray fields;
    for (int fi = 0; fi < table.fields.count(); ++fi) {
        fields.append(QJsonValue(table.fields.at(fi)));
    }
    header["fields"] = fields;
    
    QJsonArray types;
    for (int fi = 0; fi < table.types.count(); ++fi) {
        types.append(QJsonValue(table.types.at(fi)));
    }
    header["types"] = types;

    QJsonArray rows;
    for (int ri = 0, rc = table.rows.count(); ri < rc; ++ri) {
        QJsonArray row;
        for (int fi = 0, fc = table.fields.count(); fi < fc; ++fi) {
            QVariant value = table.rows.at(ri).at(fi);
            QString type = table.types.at(fi);
            
            row.append(toJsonValue(value, type));
        }
        rows.append(row);
    } 
    
    obj->insert("header", header);
    obj->insert("rows", rows);
} 

void TableConverter::tableFromObject(const QJsonObject &obj, Table *table) const
{
    table->clear();
    
    QJsonObject header = obj.value("header").toObject();
    table->name = header.value("name").toString();
    
    QJsonArray fields = header.value("fields").toArray();
    QJsonArray types = header.value("types").toArray();
    
    for (int fi = 0, fc = fields.count(); fi < fc; ++fi) {
        table->fields.append(fields.at(fi).toString());
        table->types.append(types.at(fi).toString());
    }
     
    QJsonArray rows = obj.value("rows").toArray();
    for (int ri = 0, rc = rows.count(); ri < rc; ++ri) {
        QJsonArray arr = rows.at(ri).toArray();
        QVariantList row;
        for (int fi = 0, fc = table->fields.count(); fi < fc; ++fi) {
            QJsonValueRef val = arr[fi];
            const Table::Type &type = table->types.at(fi);
            
            row.append(fromJsonValue(val, type));
        }
        table->rows.append(row);
    }
}

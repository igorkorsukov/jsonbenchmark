#include "resttablesepconverter.h"
#include <QSet>
#include <QHash>
#include <QJsonArray>
#include <QVariantList>

RestTableRefConverter::RestTableRefConverter()
{
    
}

RestTableRefConverter::~RestTableRefConverter()
{
    
}

QString RestTableRefConverter::name() const
{
    return "resttable";
}

void RestTableRefConverter::restToObject(const RestList &rests, QJsonObject *obj) const
{
    QJsonObject header;
    header["name"] = "Rests";
    
    QJsonArray fields;
    fields.append(QJsonValue("id"));
    fields.append(QJsonValue("period"));
    fields.append(QJsonValue("quantity"));
    fields.append(QJsonValue("idSku"));
    fields.append(QJsonValue("idStore"));
    fields.append(QJsonValue("idDistributor"));
    fields.append(QJsonValue("deleted"));
    
    header["fields"] = fields;
    
    QJsonArray types;
    types.append(QJsonValue("uint64"));
    types.append(QJsonValue("date"));
    types.append(QJsonValue("double"));
    types.append(QJsonValue("uint64"));
    types.append(QJsonValue("uint64"));
    types.append(QJsonValue("uint64"));
    types.append(QJsonValue("bool"));
    
    header["types"] = types;
    
    
    QSet<QDate> periodsSet;
    QSet<quint64> storesSet;
    QSet<quint64> distrsSet;
    
    foreach (const Rest &rest, rests) {
        periodsSet.insert(rest.period);
        storesSet.insert(rest.idStore);
        distrsSet.insert(rest.idDistributor);
    }
    
    QJsonArray periodsArr;
    QMap<QDate, int> periodsIdx;
    makeValToIdx(periodsSet, &periodsArr, &periodsIdx);
    
    QJsonArray storesArr;
    QMap<quint64, int> storesIdx;
    makeValToIdx(storesSet, &storesArr, &storesIdx);
    
    QJsonArray distrsArr;
    QMap<quint64, int> distrsIdx;
    makeValToIdx(distrsSet, &distrsArr, &distrsIdx);
    
    QJsonObject refs;
    refs["period"] = periodsArr;
    refs["idStore"] = storesArr;
    refs["idDistributor"] = distrsArr;
    
    header["refs"] = refs;
    
    
    QJsonArray rows;
    foreach (const Rest &rest, rests) {
        QJsonArray row;
        row.append(QJsonValue(QString::number(rest.id)));
        row.append(QJsonValue(periodsIdx.value(rest.period)));
        row.append(QJsonValue(rest.quantity));
        row.append(QJsonValue(QString::number(rest.idSku)));
        row.append(QJsonValue(storesIdx.value(rest.idStore)));
        row.append(QJsonValue(distrsIdx.value(rest.idDistributor)));
        row.append(QJsonValue(rest.deleted ? 1 : 0));
        
        rows.append(row);
    }
    
    obj->insert("header", header);
    obj->insert("rows", rows);
}

void RestTableRefConverter::restsFromObject(const QJsonObject &obj, RestList *rests) const
{
    rests->clear();
    
    QJsonObject header = obj.value("header").toObject();
    QVariantList fields = header.value("fields").toArray().toVariantList();
    QJsonObject refs = header.value("refs").toObject();
    QJsonArray periodsArr = refs.value("period").toArray();
    QJsonArray storesArr = refs.value("idStore").toArray();
    QJsonArray distrsArr = refs.value("idDistributor").toArray();
    QJsonArray rows = obj.value("rows").toArray();
    
    int id_index = fields.indexOf("id");
    int period_index = fields.indexOf("period");
    int quantity_index = fields.indexOf("quantity");
    int sku_index = fields.indexOf("idSku");
    int store_index = fields.indexOf("idStore");
    int distr_index = fields.indexOf("idDistributor");
    int deleted_index = fields.indexOf("deleted");
    
    QList<QDate> periods = toDateList(periodsArr);
    QList<quint64> stores = toIdList(storesArr);
    QList<quint64> distrs = toIdList(distrsArr);
    
    foreach (const QJsonValue &val, rows) {
        QJsonArray row = val.toArray();
        Rest rest;
        rest.id = row[id_index].toString().toULongLong();
        rest.period = periods.at(row[period_index].toInt()); 
        rest.quantity = row[quantity_index].toInt();
        rest.idSku = row[sku_index].toString().toULongLong();
        rest.idStore = stores.at(row[store_index].toInt());
        rest.idDistributor = distrs.at(row[distr_index].toInt());
        rest.deleted = row[deleted_index].toInt() == 0 ? false : true;
        
        rests->append(rest);
    }
}

void RestTableRefConverter::makeValToIdx(const QSet<QDate> &set, QJsonArray *arr, QMap<QDate, int> *idx) const
{
    int index = arr->count();
    foreach (const QDate &date, set) {
        arr->append(date.toString("yyyy-MM-dd"));
        idx->insert(date, index);
        ++index;
    }
}

void RestTableRefConverter::makeValToIdx(const QSet<quint64> &set, QJsonArray *arr, QMap<quint64, int> *idx) const
{
    int index = arr->count();
    foreach (const quint64 &id, set) {
        arr->append(QString::number(id));
        idx->insert(id, index);
        ++index;
    }
}

QList<QDate> RestTableRefConverter::toDateList(const QJsonArray &arr) const
{
    QList<QDate> list;
    list.reserve(arr.size());
    foreach (const QJsonValue &val, arr) {
        list.append(QDate::fromString(val.toString(), "yyyy-MM-dd"));
    }
    return list;
}

QList<quint64> RestTableRefConverter::toIdList(const QJsonArray &arr) const
{
    QList<quint64> list;
    list.reserve(arr.size());
    foreach (const QJsonValue &val, arr) {
        list.append(val.toString().toULongLong());
    }
    return list;
}

#include "resttabledefconverter.h"
#include <QJsonArray>
#include <QMap>
#include <QVariantList>

RestTableDefConverter::RestTableDefConverter()
{
    
}

RestTableDefConverter::~RestTableDefConverter()
{
    
}

QString RestTableDefConverter::name() const
{
    return "resttabledef";
}

void RestTableDefConverter::restToObject(const  RestList &rests, QJsonObject *obj) const
{
    QJsonArray header;
    header.append(QJsonValue("id"));
    header.append(QJsonValue("period"));
    header.append(QJsonValue("quantity"));
    header.append(QJsonValue("idSku"));
    header.append(QJsonValue("idStore"));
    header.append(QJsonValue("idDistributor"));
    header.append(QJsonValue("deleted"));
    
    
    QMap<quint64, Counter> store_vals;
    QMap<quint64, Counter> distr_vals;
    
    foreach (const Rest &rest, rests) {
        store_vals[rest.idStore].count++;  
        distr_vals[rest.idDistributor].count++;
    }
    
    quint64 store_def = maxCountId(store_vals);
    quint64 distr_def = maxCountId(distr_vals);
    QDate period_def = QDate(1900, 01, 01);
    
    QJsonArray defaults;
    defaults.append(QJsonValue(""));
    defaults.append(QJsonValue(period_def.toString("yyyy-MM-dd")));
    defaults.append(QJsonValue(""));
    defaults.append(QJsonValue(""));
    defaults.append(QJsonValue(QString::number(store_def)));
    defaults.append(QJsonValue(QString::number(distr_def)));
    defaults.append(QJsonValue(""));
    
    
    QJsonArray rows;
    foreach (const Rest &rest, rests) {
        QJsonArray row;
        row.append(QJsonValue(QString::number(rest.id)));
        row.append(QJsonValue(rest.period == period_def ? "" : rest.period.toString("yyyy-MM-dd")));
        row.append(QJsonValue(rest.quantity));
        row.append(QJsonValue(QString::number(rest.idSku)));
        row.append(QJsonValue(rest.idStore == store_def ? "" : QString::number(rest.idStore)));
        row.append(QJsonValue(rest.idDistributor == distr_def ? "" : QString::number(rest.idDistributor)));
        row.append(QJsonValue(rest.deleted ? 1 : 0));
        
        rows.append(row);
    }
    
    obj->insert("header", header);
    obj->insert("default", defaults);
    obj->insert("rows", rows);
} 

void RestTableDefConverter::restsFromObject(const QJsonObject &obj, RestList *rests) const
{
    rests->clear();
    
    QVariantList header = obj.value("header").toArray().toVariantList();
    QJsonArray defs = obj.value("default").toArray();
    QJsonArray rows = obj.value("rows").toArray();
    
    int id_index = header.indexOf("id");
    int period_index = header.indexOf("period");
    int quantity_index = header.indexOf("quantity");
    int sku_index = header.indexOf("idSku");
    int store_index = header.indexOf("idStore");
    int distr_index = header.indexOf("idDistributor");
    int deleted_index = header.indexOf("deleted");
    
    QDate periodDef = QDate::fromString(defs.at(period_index).toString(), "yyyy-MM-dd");
    quint64 storeDef = defs.at(store_index).toString().toULongLong();
    quint64 distrDef = defs.at(distr_index).toString().toULongLong();
    
    foreach (const QJsonValue &val, rows) {
        QJsonArray row = val.toArray();
        Rest rest;
        rest.id = row.at(id_index).toString().toULongLong();
        rest.period = dateValue(row.at(period_index), periodDef); 
        rest.quantity = row.at(quantity_index).toInt();
        rest.idSku = row.at(sku_index).toString().toULongLong();
        rest.idStore = idValue(row.at(store_index), storeDef);
        rest.idDistributor = idValue(row.at(distr_index), distrDef);
        rest.deleted = row.at(deleted_index).toInt() == 0 ? false : true;
        
        rests->append(rest);
    }
}

QDate RestTableDefConverter::dateValue(const QJsonValue &val, const QDate &def) const
{
    QString valStr = val.toString();
    return valStr.isEmpty() ? def : QDate::fromString(valStr, "yyyy-MM-dd");
}

quint64 RestTableDefConverter::idValue(const QJsonValue &val, const quint64 &def) const
{
    QString valStr = val.toString();
    return valStr.isEmpty() ? def : valStr.toULongLong();
}

quint64 RestTableDefConverter::maxCountId(const QMap<quint64, Counter> &map) const
{
    quint64 id = 0;
    int max_count = 0;
    for (QMap<quint64, Counter>::ConstIterator it = map.begin(), end = map.end(); it != end;  ++it) {
        if (it.value().count > max_count) {
            max_count = it.value().count;
            id = it.key();
        }
    }
    
    return id;
}

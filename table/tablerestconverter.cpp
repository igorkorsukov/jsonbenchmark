#include "tablerestconverter.h"
#include <QSet>
#include <QHash>
#include <QJsonArray>
#include <QJsonValue>
#include <QDate>
#include <QDebug>

#include <QTime>
#define TIME(str) qDebug() << str << ":" << time.elapsed() << "ms"

TableRestConverter::TableRestConverter()
{
    
}

TableRestConverter::~TableRestConverter()
{
    
}

QString TableRestConverter::name() const
{
    return "tablerest";
}

void TableRestConverter::tableToObject(const  Table &table, QJsonObject *obj) const
{
    QJsonObject header;
    header["name"] = table.name;
    
    QJsonArray fields;
    QJsonArray types;
    for (int fi = 0; fi < table.fields.count(); ++fi) {
        fields.append(QJsonValue(table.fields.at(fi)));
        types.append(QJsonValue(table.types.at(fi)));
    }
    header["fields"] = fields;
    header["types"] = types;
    
    QString periodField("period");
    QString storeField("idStore");
    QString distrField("idDistributor");
    
    QSet<QDate> periodsSet;
    QSet<quint64> storesSet;
    QSet<quint64> distrsSet;
    
    for (int ri = 0, rc = table.rows.count(); ri < rc; ++ri) {
        for (int fi = 0, fc = table.fields.count(); fi < fc; ++fi) {
            const QString &field = table.fields.at(fi);
            
            if (periodField == field) {
                const QVariant &val = table.rows.at(ri).at(fi);
                periodsSet.insert(val.toDate());
            } else if (storeField == field) {
                const QVariant &val = table.rows.at(ri).at(fi);
                storesSet.insert(val.toULongLong());
            } else if (distrField == field) {
                const QVariant &val = table.rows.at(ri).at(fi);
                distrsSet.insert(val.toULongLong());
            }
        }
    }
    
    QJsonArray periodsArr;
    QMap<QDate, int> periodsIdx;
    makeValToIdx(periodsSet, &periodsArr, &periodsIdx);
    
    QJsonArray storesArr;
    QMap<quint64, int> storesIdx;
    makeValToIdx(storesSet, &storesArr, &storesIdx);
    
    QJsonArray distrsArr;
    QMap<quint64, int> distrsIdx;
    makeValToIdx(distrsSet, &distrsArr, &distrsIdx);
    
    QJsonObject refs;
    refs["period"] = periodsArr;
    refs["idStore"] = storesArr;
    refs["idDistributor"] = distrsArr;
    
    header["refs"] = refs;
    
    QJsonArray rows;
    for (int ri = 0, rc = table.rows.count(); ri < rc; ++ri) {
        QJsonArray row;
        for (int fi = 0, fc = table.fields.count(); fi < fc; ++fi) {
            QVariant val = table.rows.at(ri).at(fi);
            QString field = table.fields.at(fi);
            QString type = table.types.at(fi);
            
            if (periodField == field) {
                row.append(QJsonValue(periodsIdx.value(val.toDate())));
            } else if (storeField == field) {
                row.append(QJsonValue(storesIdx.value(val.toULongLong())));
            } else if (distrField == field) {
                row.append(QJsonValue(distrsIdx.value(val.toULongLong())));
            } else {
                row.append(toJsonValue(val, type));
            }
        }
        rows.append(row);
    } 
    
    obj->insert("header", header);
    obj->insert("rows", rows);
}

void TableRestConverter::tableFromObject(const QJsonObject &obj, Table *table) const
{
    table->clear();
    
    QTime time;
    time.start();
    
    QJsonObject header = obj.value("header").toObject();
    table->name = header.value("name").toString();
    
    QJsonArray fields = header.value("fields").toArray();
    QJsonArray types = header.value("types").toArray();
    
    for (int fi = 0, fc = fields.count(); fi < fc; ++fi) {
        table->fields.append(fields.at(fi).toString());
        table->types.append(types.at(fi).toString());
    }
    TIME("fields and types");
    
    QJsonObject refs = header.value("refs").toObject();
    QJsonArray periodsArr = refs.value("period").toArray();
    QJsonArray storesArr = refs.value("idStore").toArray();
    QJsonArray distrsArr = refs.value("idDistributor").toArray();
    
    QList<QDate> periods;
    QList<quint64> stores;
    QList<quint64> distrs;
    
    makeValFromIdx(periodsArr, &periods);
    makeValFromIdx(storesArr, &stores);
    makeValFromIdx(distrsArr, &distrs);
    
    TIME("makeValFromIdx");

    QJsonArray rows = obj.value("rows").toArray();
    table->rows.reserve(rows.size());
    
    QString periodField("period");
    QString storeField("idStore");
    QString distrField("idDistributor");
    
    for (int ri = 0, rc = rows.count(); ri < rc; ++ri) {
        QJsonArray arr = rows.at(ri).toArray();
        QVariantList row;
        for (int fi = 0, fc = table->fields.count(); fi < fc; ++fi) {
            QJsonValueRef val = arr[fi];
            const QString &field = table->fields.at(fi);
            const Table::Type &type = table->types.at(fi);
            
            if (periodField == field) {
                row.append(periods.at(val.toInt()));
            } else if (storeField == field) {
                row.append(stores.at(val.toInt()));
            } else if (distrField == field) {
                row.append(distrs.at(val.toInt()));
            } else {
                row.append(fromJsonValue(val, type));
            }
        }
        table->rows.append(row);
    }
    
    TIME("rows");
}

template <typename T>
void TableRestConverter::makeValToIdx(const QSet<T> &set, QJsonArray *arr, QMap<T, int> *idx) const
{
    int index = arr->count();
    foreach (const T &val, set) {
        arr->append(toJsonValue(val));
        idx->insert(val, index);
        ++index;
    }
}

template <typename T>
void TableRestConverter::makeValFromIdx(QJsonArray &arr, QList<T> *idx) const
{
    for (int i = 0, c = arr.count(); i < c; ++i) {
        QJsonValueRef val = arr[i];
        idx->append(fromJsonValue(val, T()));
    }
}

#ifndef RESTS
#define RESTS

#include <QList>
#include <QDate>

struct Rest {
    quint64 id;
    QDate period; 
    int quantity;
    quint64 idSku;
    quint64 idStore;
    quint64 idDistributor;
    bool deleted;
};

typedef QList<Rest> RestList;

#endif // RESTS


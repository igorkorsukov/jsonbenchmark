#ifndef TABLEGENERATOR_H
#define TABLEGENERATOR_H

#include "table.h"
#include <QDate>

class TableGenerator
{
public:
    TableGenerator();
    ~TableGenerator();
    
    void generate(Table *table) const;
    
private:
    QDate period(int idx) const;
};

#endif // TABLEGENERATOR_H

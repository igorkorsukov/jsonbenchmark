#-------------------------------------------------
#
# Project created by QtCreator 2015-01-15T23:09:09
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = jsonbenchmark
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    rests/iconverter.cpp \
    rests/restgenerator.cpp \
    table/table.cpp \
    table/converter.cpp \
    table/tableconverter.cpp \
    parser.cpp \
    table/tablegenerator.cpp \
    rests/resttableconverter.cpp \
    rests/resttabledefconverter.cpp \
    rests/resttablesepconverter.cpp \
    rests/restobjectsconverter.cpp \
    table/tablerefconverter.cpp \
    table/tablerestconverter.cpp

HEADERS += \
    rests/rests.h \
    rests/iconverter.h \
    rests/restgenerator.h \
    table/table.h \
    table/converter.h \
    table/tableconverter.h \
    parser.h \
    table/tablegenerator.h \
    rests/resttableconverter.h \
    rests/resttabledefconverter.h \
    rests/resttablesepconverter.h \
    rests/restobjectsconverter.h \
    table/tablerefconverter.h \
    table/tablerestconverter.h

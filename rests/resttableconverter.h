#ifndef RESTTABLEPLANTEST_H
#define RESTTABLEPLANTEST_H

#include "iconverter.h"
#include <QHash>
#include <QDate>

class RestTableConverter: public RestConverter
{
public:
    RestTableConverter();
    ~RestTableConverter();
    
    QString name() const;
    void restToObject(const RestList &rests, QJsonObject *obj) const;
    void restsFromObject(const QJsonObject &obj, RestList *rests) const;
    
private:
    
    QDate dateVal(const QJsonValue &val) const;
    
    mutable QHash<QString, QDate> m_dateCache;
};

#endif // RESTTABLEPLANTEST_H

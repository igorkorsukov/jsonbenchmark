#include <QCoreApplication>
#include <QDateTime>
#include <QVariant>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>
#include <QFile>
#include <QDebug>

#include "parser.h"

#include "rests/rests.h"
#include "rests/restgenerator.h"
#include "rests/iconverter.h"
#include "rests/restobjectsconverter.h"
#include "rests/resttableconverter.h"
#include "rests/resttabledefconverter.h"
#include "rests/resttablesepconverter.h"

#include "table/table.h"
#include "table/converter.h"
#include "table/tablegenerator.h"
#include "table/tableconverter.h"
#include "table/tablerefconverter.h"
#include "table/tablerestconverter.h"

int STR_WIDTH = 24;
#define FORMAT(str, width) QString(str).leftJustified(width, ' ').toLatin1().constData()

bool IsCompact = true;

void example(const RestList &rests, const RestConverter *conv)
{
    qDebug() << "\nExample" << conv->name();
    
    QJsonObject obj;
    conv->restToObject(rests, &obj);
    
    QByteArray ba;
    Parser::serialize(obj, &ba, false);
    
    qDebug() << ba.constData();
}

void example(const Table &table, const Converter *conv)
{
    qDebug() << "\nExample" << conv->name();
    
    QJsonObject obj;
    conv->tableToObject(table, &obj);
    
    QByteArray ba;
    Parser::serialize(obj, &ba, false);
    
    qDebug() << ba.constData();
}

struct Result {
    QString name;
    quint64 write_total;
    quint64 read_total;
    quint64 total() const { return write_total + read_total; }
    quint64 size;
    bool isEqual;
    Result() : write_total(0), read_total(0), size(0), isEqual(true) {}
};

Result test(RestList &rests, const RestConverter *conv)
{
    const QString FILEPATH(qApp->applicationDirPath() + "/" + conv->name() + ".json");
    
    QTime time;
    quint64 step = 0;
    quint64 step_total = 0;
    Result ret;
    ret.name = conv->name();
    
#define TIME(str) \
    step = time.elapsed(); \
    step_total += step; \
    qDebug() << FORMAT(QString(str) + conv->name() + ":", STR_WIDTH) << step << "ms"; \
    time.start();
    
    time.start();
    qDebug() << "\nWrite" << conv->name();
    QJsonObject obj;
    conv->restToObject(rests, &obj);
    TIME("rests to ");
    
    QByteArray ba;
    Parser::serialize(obj, &ba, IsCompact);
    TIME("serialize ");
    
    ret.size = Parser::write_file(FILEPATH, ba);
    TIME("write_file ");
    
    ret.write_total = step_total;
    step_total = 0;
    
    qDebug() << "\nRead" << conv->name();
    Parser::read_file(FILEPATH, &ba);
    TIME("read_file ");
    
    Parser::parse(ba, &obj);
    TIME("parse ");
    
    conv->restsFromObject(obj, &rests);
    TIME("rests from ");
    
    ret.read_total = step_total;
    
    return ret;
}

Result test(const Table &srctable, const Converter *conv)
{
    const QString FILEPATH(qApp->applicationDirPath() + "/" + conv->name() + ".json");
    
    QTime time;
    quint64 step = 0;
    quint64 step_total = 0;
    Result ret;
    ret.name = conv->name();
    
#define TIME(str) \
    step = time.elapsed(); \
    step_total += step; \
    qDebug() << FORMAT(QString(str) + conv->name() + ":", STR_WIDTH) << step << "ms"; \
    time.start();
    
    time.start();
    qDebug() << "\nWrite" << conv->name();
    QJsonObject obj;
    conv->tableToObject(srctable, &obj);
    TIME("table to ");
    
    QByteArray ba;
    Parser::serialize(obj, &ba, IsCompact);
    TIME("serialize ");
    
    ret.size = Parser::write_file(FILEPATH, ba);
    TIME("write_file ");
    
    ret.write_total = step_total;
    step_total = 0;
    
    qDebug() << "\nRead" << conv->name();
    Parser::read_file(FILEPATH, &ba);
    TIME("read_file ");
    
    Parser::parse(ba, &obj);
    TIME("parse ");
    
    Table readtable;
    conv->tableFromObject(obj, &readtable);
    TIME("table from ");
    
    ret.read_total = step_total;
    
    ret.isEqual = srctable.isEqual(readtable);   
    
    return ret;
}

void total(const Result &ret, const Result &base)
{
#define RATE(f, s) " - " << (100.0 - ((double)s * 100.0 / (double)f)) << "% or" << ((double)f / (double)s) << "times"
#define TITLE(str) FORMAT(ret.name + QString(str), STR_WIDTH)
#define VALUE(val, unit) FORMAT(QString::number(val) + unit, 15)
    
    qDebug() << "\nTotal" << ret.name;
    qDebug() << TITLE(" write total:")  << VALUE(ret.write_total, " ms")    << RATE(base.write_total, ret.write_total); 
    qDebug() << TITLE(" read total:")   << VALUE(ret.read_total, " ms")     << RATE(base.read_total, ret.read_total); 
    qDebug() << TITLE(" total:")        << VALUE(ret.total(), " ms")        << RATE(base.total(), ret.total()); 
    qDebug() << TITLE(" file size:")    << VALUE(ret.size, " bytes")        << RATE(base.size, ret.size); 
    qDebug() << TITLE(" is equal:")     << VALUE(ret.isEqual, "");
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    
    qDebug() << "Start";
    RestList rests;
    RestGenerator rg;
    rg.generate(&rests);
    
    Table table;
    TableGenerator tg;
    tg.generate(&table);
    
    QList<RestConverter*> restconvs;
    //restconvs.append(new RestObjectsConverter());
    //restconvs.append(new RestTableConverter());
    //restconvs.append(new RestTableDefConverter()); 
    restconvs.append(new RestTableRefConverter());
    
    QList<Converter*> tableconvs;
    //tableconvs.append(new TableConverter());
    tableconvs.append(new TableRefConverter());
    tableconvs.append(new TableRestConverter());
    
    RestList exmrests;
    exmrests << rests.at(1);
    foreach (const RestConverter *conv, restconvs) {
        example(exmrests, conv);
    }
    
    Table exmtable;
    exmtable.name = table.name;
    exmtable.fields = table.fields;
    exmtable.types = table.types;
    exmtable.rows.append(table.rows.at(1));
    foreach (const Converter *conv, tableconvs) {
        example(exmtable, conv);
    }
    
    IsCompact = false;
    
    QList<Result> results;
    foreach (const RestConverter *conv, restconvs) {
        Result ret = test(rests, conv);
        results.append(ret);
    }
    
    foreach (const Converter *conv, tableconvs) {
        Result ret = test(table, conv);
        results.append(ret);
    }
    
    Result base = results.first();
    foreach (const Result &ret, results) {
        total(ret, base);
    }
    
    qDeleteAll(restconvs);
    qDeleteAll(tableconvs);
    
    return a.exec();
}

#include "resttableconverter.h"
#include <QJsonArray>
#include <QVariantList>

RestTableConverter::RestTableConverter()
{
    
}

RestTableConverter::~RestTableConverter()
{
    
}

QString RestTableConverter::name() const
{
    return "resttable";
}

void RestTableConverter::restToObject(const RestList &rests, QJsonObject *obj) const
{
    QJsonArray header;
    header.append(QJsonValue("id"));
    header.append(QJsonValue("period"));
    header.append(QJsonValue("quantity"));
    header.append(QJsonValue("idSku"));
    header.append(QJsonValue("idStore"));
    header.append(QJsonValue("idDistributor"));
    header.append(QJsonValue("deleted"));

    
    QJsonArray rows;
    foreach (const Rest &rest, rests) {
        QJsonArray row;
        row.append(QJsonValue(QString::number(rest.id)));
        row.append(QJsonValue(rest.period.toString("yyyy-MM-dd")));
        row.append(QJsonValue(rest.quantity));
        row.append(QJsonValue(QString::number(rest.idSku)));
        row.append(QJsonValue(QString::number(rest.idStore)));
        row.append(QJsonValue(QString::number(rest.idDistributor)));
        row.append(QJsonValue(rest.deleted ? 1 : 0));

        rows.append(row);
    }
    
    obj->insert("header", header);
    obj->insert("rows", rows);
} 

void RestTableConverter::restsFromObject(const QJsonObject &obj, RestList *rests) const
{
    rests->clear();
    
    QVariantList header = obj.value("header").toArray().toVariantList();
    QJsonArray rows = obj.value("rows").toArray();
    
    int id_index = header.indexOf("id");
    int period_index = header.indexOf("period");
    int quantity_index = header.indexOf("quantity");
    int idSku_index = header.indexOf("idSku");
    int idStore_index = header.indexOf("idStore");
    int idDistributor_index = header.indexOf("idDistributor");
    int deleted_index = header.indexOf("deleted");
    
    foreach (const QJsonValue &val, rows) {
        QJsonArray row = val.toArray();
        Rest rest;
        rest.id = row.at(id_index).toString().toULongLong();
        rest.period = QDate::fromString(row.at(period_index).toString(), "yyyy-MM-dd"); 
        rest.quantity = row.at(quantity_index).toInt();
        rest.idSku = row.at(idSku_index).toString().toULongLong();
        rest.idStore = row.at(idStore_index).toString().toULongLong();
        rest.idDistributor = row.at(idDistributor_index).toString().toULongLong();
        rest.deleted = row.at(deleted_index).toInt() == 0 ? false : true;
        
        rests->append(rest);
    }
}

QDate RestTableConverter::dateVal(const QJsonValue &val) const
{
    QString valStr = val.toString();
    QDate date = m_dateCache.value(valStr);
    if (date.isNull()) {
        date = QDate::fromString(valStr, "yyyy-MM-dd");
        m_dateCache.insert(valStr, date);
    }
    return date;
}

#include "parser.h"
#include <QJsonDocument>
#include <QFile>

Parser::Parser()
{
    
}

Parser::~Parser()
{
    
}

void Parser::serialize(const QJsonObject &obj, QByteArray *ba, bool compact)
{
    QJsonDocument doc(obj);
    *ba = doc.toJson(compact ? QJsonDocument::Compact : QJsonDocument::Indented);
}

void Parser::parse(const QByteArray &ba, QJsonObject *obj)
{
    QJsonDocument doc = QJsonDocument::fromJson(ba);
    *obj = doc.object();
}

quint64 Parser::write_file(const QString &filePath, const QByteArray &ba)
{
    QFile file(filePath);
    
    if (file.exists()) {
        file.remove();
    }
    
    file.open(QIODevice::WriteOnly);
    file.write(ba);
    
    return file.size();
}

void Parser::read_file(const QString &filePath, QByteArray *ba)
{
    QFile file(filePath);
    file.open(QIODevice::ReadOnly);
    *ba = file.readAll();
}

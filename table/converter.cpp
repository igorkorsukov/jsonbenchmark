#include "converter.h"
#include <QDate>

Converter::Converter()
{
    
}

Converter::~Converter()
{
    
}

QJsonValue Converter::toJsonValue(const QVariant &val, const Table::Type &type) const
{
    if (Table::Uint64 == type)  return toJsonValue(val.toULongLong());
    if (Table::Double == type)  return QJsonValue(val.toDouble());
    if (Table::Bool == type)    return QJsonValue(val.toBool() ? 1 : 0);
    if (Table::Date == type)    return toJsonValue(val.toDate());
    
    return QJsonValue();
}

QVariant Converter::fromJsonValue(const QJsonValueRef &val, const Table::Type &type) const
{
    if (Table::Uint64 == type)  return QVariant(fromJsonValue(val, quint64()));
    if (Table::Double == type)  return QVariant(val.toDouble());
    if (Table::Bool == type)    return QVariant(val.toInt() == 0 ? false : true);
    if (Table::Date == type)    return QVariant(fromJsonValue(val, QDate()));
    
    return QVariant();
}

QJsonValue Converter::toJsonValue(const quint64 &val) const
{
    return QJsonValue(QString::number(val));
}

QJsonValue Converter::toJsonValue(const QDate &val) const
{
    return QJsonValue(val.toString("yyyy-MM-dd"));
}

quint64 Converter::fromJsonValue(const QJsonValueRef &val, const quint64 &t) const
{
    Q_UNUSED(t);
    return val.toString().toULongLong();
}

QDate Converter::fromJsonValue(const QJsonValueRef &val, const QDate &t) const
{
    Q_UNUSED(t);
    return QDate::fromString(val.toString(), "yyyy-MM-dd");
}

bool Converter::isType(const Table::Type &type, const quint64 &t) const
{
    Q_UNUSED(t);
    return Table::Uint64 == type;
}

bool Converter::isType(const Table::Type &type, const QDate &t) const
{
    Q_UNUSED(t);
    return Table::Date == type;
}

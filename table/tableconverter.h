#ifndef TABLECONVERTER_H
#define TABLECONVERTER_H

#include "converter.h"

class TableConverter : public Converter
{
public:
    TableConverter();
    ~TableConverter();
    
    virtual QString name() const;
    virtual void tableToObject(const Table &table, QJsonObject *obj) const;
    virtual void tableFromObject(const QJsonObject &obj, Table *table) const;
};

#endif // TABLECONVERTER_H

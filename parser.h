#ifndef PARSER_H
#define PARSER_H

#include <QJsonObject>
#include <QByteArray>

class Parser
{
public:
    Parser();
    ~Parser();
    
    static void serialize(const QJsonObject &obj, QByteArray *ba, bool compact = true);
    static void parse(const QByteArray &ba, QJsonObject *obj);
    
    static quint64 write_file(const QString &filePath, const QByteArray &ba);
    static void read_file(const QString &filePath, QByteArray *ba);
};

#endif // PARSER_H

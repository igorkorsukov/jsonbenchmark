#include "tablegenerator.h"

TableGenerator::TableGenerator()
{
    
}

TableGenerator::~TableGenerator()
{
    
}

void TableGenerator::generate(Table *table) const
{
    table->name = "Rests";
    table->fields.clear();
    table->types.clear();
    table->rows.clear();
    table->fields << "id" << "period" << "quantity" << "idSku" << "idStore" << "idDistributor" << "deleted";
    table->types << Table::Uint64 << Table::Date << Table::Double << Table::Uint64 << Table::Uint64 << Table::Uint64 << Table::Bool;
    
    quint64 baseId = 13230934518000000ULL;
    int count = 100000;
    int half = count / 2;
    for (int i = 0; i < count; ++i) {
        QVariantList row;
        row << baseId + i
            << period(i)
            << i
            << baseId + i
            << (i < half ? baseId - 2 : baseId - 3)
            << baseId - 1
            << false;
        
        table->rows.append(row);
    }
}

QDate TableGenerator::period(int idx) const
{
    bool is1= idx % 100 == 0;
    bool is2 = idx % 500 == 0;
    
    if (is2) {
        return QDate(2015, 01, 01);
    } else if (is1) {
        return QDate(2014, 01, 01);
    } 
    
    return QDate(1900, 01, 01);
}

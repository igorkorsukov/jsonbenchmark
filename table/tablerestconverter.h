#ifndef TABLERESTCONVERTER_H
#define TABLERESTCONVERTER_H

#include "converter.h"

class TableRestConverter : public Converter
{
public:
    TableRestConverter();
    ~TableRestConverter();
    
    virtual QString name() const;
    virtual void tableToObject(const  Table &table, QJsonObject *obj) const;
    virtual void tableFromObject(const QJsonObject &obj, Table *table) const;
    
private:
    template <typename T>
    void makeValToIdx(const QSet<T> &set, QJsonArray *arr, QMap<T, int> *idx) const;
    
    template <typename T>
    void makeValFromIdx(QJsonArray &arr, QList<T> *idx) const;
};

#endif // TABLERESTCONVERTER_H

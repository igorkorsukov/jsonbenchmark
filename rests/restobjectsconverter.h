#ifndef RESTOBJECTSTEST_H
#define RESTOBJECTSTEST_H

#include "iconverter.h"

class RestObjectsConverter: public RestConverter
{
public:
    RestObjectsConverter();
    ~RestObjectsConverter();
    
    virtual QString name() const;
    virtual void restToObject(const  RestList &rests, QJsonObject *obj) const;
    virtual void restsFromObject(const QJsonObject &obj, RestList *rests) const;
};

#endif // vOBJECTSTEST_H

#ifndef ICONVERTER_H
#define ICONVERTER_H

#include <QJsonObject>
#include "rests.h"

class RestConverter
{
public:
    RestConverter();
    virtual ~RestConverter();
    
    virtual QString name() const = 0;
    virtual void restToObject(const RestList &rests, QJsonObject *obj) const = 0;
    virtual void restsFromObject(const QJsonObject &obj, RestList *rests) const = 0;
};

#endif // ICONVERTER_H

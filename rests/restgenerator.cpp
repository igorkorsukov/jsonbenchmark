#include "restgenerator.h"

RestGenerator::RestGenerator()
{
    
}

RestGenerator::~RestGenerator()
{
    
}

void RestGenerator::generate(RestList *list) const
{
    quint64 baseId = 13230934518000000ULL;
    int count = 100000;
    int half = count / 2;
    for (int i = 0; i < count; ++i) {
        Rest rest;
        rest.id = baseId + i;
        rest.period = period(i); 
        rest.quantity = i;
        rest.idSku = baseId + i;
        rest.idStore =  i < half ? baseId - 2 : baseId - 3;
        rest.idDistributor = baseId - 1;
        rest.deleted = false;
        
        list->append(rest);
    }
}

QDate RestGenerator::period(int idx) const
{
    bool is1= idx % 100 == 0;
    bool is2 = idx % 500 == 0;
    
    if (is2) {
        return QDate(2015, 01, 01);
    } else if (is1) {
        return QDate(2014, 01, 01);
    } 
    
    return QDate(1900, 01, 01);
}
